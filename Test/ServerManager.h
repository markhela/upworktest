//
//  ServerManager.h
//  Test
//
//  Created by Артем Мархель on 14.09.17.
//  Copyright © 2017 Артем Мархель. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CompletionBlock)(BOOL success, id response);

@interface ServerManager : NSObject

+ (ServerManager*)shared;

- (void)getWeatherForCity:(NSString*)city completion:(CompletionBlock)completion;

@end
