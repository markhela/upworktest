//
//  ViewController.m
//  Test
//
//  Created by Артем Мархель on 14.09.17.
//  Copyright © 2017 Артем Мархель. All rights reserved.
//

#import "ViewController.h"
#import "City.h"
#import "ServerManager.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UILabel *weatherLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeight;
@property (weak, nonatomic) IBOutlet UIView *topView;

@property (strong, nonatomic) NSMutableArray* cities;
@property (assign, nonatomic) BOOL isExpand;

@end

@implementation ViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.tableFooterView = [[UIView alloc] init];
    
    self.cities = [NSMutableArray array];
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"json"];
    NSData* data = [NSData dataWithContentsOfFile:path];
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSArray* dataArray = json[@"data"];
    for (NSDictionary* item in dataArray) {
        City* city = [[City alloc] initWithDictionary:item];
        [self.cities addObject:city];
    }
    
    self.viewHeight.constant = self.view.bounds.size.height / 3;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark - Actions

- (IBAction)viewTap:(UITapGestureRecognizer *)sender {
    if (!self.isExpand) {
        [UIView animateWithDuration:1.0 animations:^{
            self.viewHeight.constant = self.view.bounds.size.height * 0.6;
            [self.view layoutIfNeeded];
        }];
        self.isExpand = YES;
    } else {
        [UIView animateWithDuration:1.0 animations:^{
            self.viewHeight.constant = self.view.bounds.size.height / 3;
            [self.view layoutIfNeeded];
        }];
        self.isExpand = NO;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* identifier = @"cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    City* city = self.cities[indexPath.row];
    cell.textLabel.text = city.name;
    cell.detailTextLabel.text = city.code;
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    City* city = self.cities[indexPath.row];
    
    self.descLabel.text = city.desc;
    
    [[ServerManager shared] getWeatherForCity:city.name completion:^(BOOL success, id response) {
       
        if (success) {
            
            NSDictionary* main = response[@"main"];
            NSInteger temp = [main[@"temp"] integerValue];
            
            NSDictionary* weather = [response[@"weather"] firstObject];
            NSString* desc = weather[@"description"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tempLabel.text = [NSString stringWithFormat:@"%ld", temp];
                self.weatherLabel.text = desc;
            });
            
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:response message:nil preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"ОК" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
        
    }];
}

@end
