//
//  City.m
//  Test
//
//  Created by Артем Мархель on 14.09.17.
//  Copyright © 2017 Артем Мархель. All rights reserved.
//

#import "City.h"

@implementation City

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if (self) {
        self.code = dictionary[@"code"];
        self.desc = dictionary[@"description"];
        self.name = dictionary[@"name"];
    }
    return self;
}

@end
