//
//  ServerManager.m
//  Test
//
//  Created by Артем Мархель on 14.09.17.
//  Copyright © 2017 Артем Мархель. All rights reserved.
//

#import "ServerManager.h"
#import <AFNetworking.h>

@interface ServerManager()

@property (strong, nonatomic) AFHTTPSessionManager* sessionManager;

@end

@implementation ServerManager

+ (ServerManager*)shared {
    
    static ServerManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ServerManager alloc] init];
    });
    return manager;
}

- (AFHTTPSessionManager *)sessionManager {
    if (!_sessionManager) {
        _sessionManager = [AFHTTPSessionManager manager];
        [_sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [_sessionManager.requestSerializer setTimeoutInterval:160];
    }
    return _sessionManager;
}

- (void)getWeatherForCity:(NSString*)city completion:(CompletionBlock)completion {

    [self.sessionManager GET:[NSString stringWithFormat:@"https://api.openweathermap.org/data/2.5/weather?q=%@&units=metric&APPID=a0eb13c4a12d429e29547d6158416bad", city] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"GET WEATHER: %@", responseObject);
        completion(YES, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"GET WEATHER: %@", [error localizedDescription]);
        completion(NO, [error localizedDescription]);
        
    }];
}

@end
