//
//  City.h
//  Test
//
//  Created by Артем Мархель on 14.09.17.
//  Copyright © 2017 Артем Мархель. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSString* desc;
@property (strong, nonatomic) NSString* name;

- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
